﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float MovmentSpeed;
    public float Damage;
    public Vector3 initpos;
	
	void Start ()
    {
        initpos = transform.position;
	}
	
	
	void Update ()
    {
        transform.Translate(Vector3.right * MovmentSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, initpos) > 20)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider col)
    {

        if(col.tag == "enemy")
        {
            col.GetComponent<Health>().health -= Damage;
            Destroy(gameObject);
        }
    }
}
