﻿using UnityEngine;
using System.Collections;

public class MoneyLabel : MonoBehaviour {

    // Use this for initialization
    private Money mscr;
    void Start()
    {
        mscr = GameObject.Find("GameLogic").GetComponent<Money>();
    }
	void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, 100, 30));

        GUILayout.BeginHorizontal();
        GUILayout.Label("Money: " + mscr.money);
        GUILayout.EndHorizontal();

        GUILayout.EndArea();
    }
}
