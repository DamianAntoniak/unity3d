﻿using UnityEngine;
using System.Collections;

public class SetTower : MonoBehaviour {

    
    public int Selected;
    public GameObject[] towers;
    public float[] prices;
    public GameObject Field;
    private Money mscr;
	
	void Start ()
    {
        mscr = GameObject.Find("GameLogic").GetComponent<Money>();
	}
	
	
	void Update ()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, 9))
        {
            if(hit.transform.tag == "field")
            {
                Field = hit.transform.gameObject;
            }
            else
            {
                Field = null;
            }
        }
        
        if (Input.GetMouseButtonDown(0) && Field != null)
        {
            
            FieldTaken fscr = Field.GetComponent<FieldTaken>();
            if(!fscr.isTaken && mscr.money >= prices[Selected])
            {
                mscr.money -= prices[Selected];
                Vector3 pos = new Vector3(Field.transform.position.x, 1f, Field.transform.position.z);
                fscr.Tower = (GameObject)Instantiate(towers[Selected], pos, Quaternion.identity);
                fscr.isTaken = true;

            }
        }
	}
}
