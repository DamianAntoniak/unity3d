﻿using UnityEngine;
using System.Collections;

public class WaveManager : MonoBehaviour
{

    public int NumOut;
    public GameObject[] Enemies;
    public float Cooldown;
    private float cd;

    public float[] positions;

	void Start ()
    {
        cd = Cooldown * 2;
	}
	
	
	void Update ()
    {
	    if(cd > 0)
        {
            cd -= Time.deltaTime;
        }
        else
        {
            cd = Cooldown;
            int index = Random.Range(0, 5);
            Vector3 pos = new Vector3(7.1f, 0.84f, positions[index]);
            index = Random.Range(0, Enemies.Length);
            Instantiate(Enemies[index], pos, Quaternion.identity);
        }
	}
}
