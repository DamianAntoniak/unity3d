﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour
{
    public float Cooldown;
    private float cd;
    public GameObject projectile;


	void Start ()
    {
        cd = Cooldown;
	}
	


	void Update ()
    {
	    if(cd > 0)
        {
            cd -= Time.deltaTime;
        }
        else
        {

            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.right, out hit, 20))
            {
                
                if (hit.transform.tag == "enemy")
                {
                    cd = Cooldown;
                    Instantiate(projectile, transform.position, Quaternion.identity);
                }
            }
        }
	}
}
