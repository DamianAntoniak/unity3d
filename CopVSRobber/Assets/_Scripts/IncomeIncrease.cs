﻿using UnityEngine;
using System.Collections;

public class IncomeIncrease : MonoBehaviour
{
    public float Cooldown;
    private float cd;
    public float income;
    private Money mscr;

	
	void Start ()
    {
        mscr = GameObject.Find("GameLogic").GetComponent<Money>();
	}
	

	void Update ()
    {
	    if(cd > 0)
        {
            cd -= Time.deltaTime;
        }
        else
        {
            cd = Cooldown;
            mscr.money += income;
        }
	}
}
