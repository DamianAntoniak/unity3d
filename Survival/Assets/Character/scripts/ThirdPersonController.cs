﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


[AddComponentMenu("Camera-Control/Mouse Orbit")]
public class ThirdPersonController : NetworkBehaviour
{


    public Transform target;
    public float distance = 10.0f;

    public float xSpeed = 250.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20.0f;
    public float yMaxLimit = 80.0f;

    public float zoomRate = 20.0f;

    private double x = 0.0f;
    private double y = 0.0f;


	void Start ()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        if(GetComponent<Rigidbody>())
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }
		
	}
	
	void LateUpdate()
    {
        if (!isLocalPlayer) return;
        if (target)
        {
            if (Input.GetMouseButton(0))
            {
                x += Input.GetAxis("Mouse X") * xSpeed * 0.02;
                y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02;
            }

            distance += -(Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime) * zoomRate * Mathf.Abs(distance);

            if (distance < 2.5) distance = 2.5f;
            if (distance > 20) distance = 20;

            y = ClampAngle((float)y, yMinLimit, yMaxLimit);

            if (y == yMinLimit  /*&& */) distance += -(Input.GetAxis("Mouse Y") * Time.deltaTime) * Mathf.Abs(distance);
            Quaternion rotation = Quaternion.Euler((float)x, (float)y, 0f);
            Vector3 position = rotation * new Vector3(0.0f, 2.0f, -distance) + target.position;

            transform.rotation = rotation;
            transform.position = position;

        }
    }

    static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360) angle += 360;
        if (angle > 360) angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }
}
