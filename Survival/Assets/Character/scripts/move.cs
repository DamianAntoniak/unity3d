﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class move : NetworkBehaviour
{

    //http://forum.unity3d.com/threads/16949-WOW-Camera-Movement?highlight=wow+camera
    public float turnSpeed = 10.0f;
    public float moveSpeed = 10.0f;
    public int mouseTurnMultiplier = 1;

    private float x;
    private float z;

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;
        x = 0.0f;

        // check to see if the W or S key is being pressed.  
        z = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        // Move the character forwards or backwards
        transform.Translate(0, 0, z);

        // Check to see if the A or S key are being pressed
        //if(Input.GetAxis("Horizontal"))
        if (Input.GetAxis("Horizontal") == 1)
        {
            // Get the A or S key (-1 or 1)
            x = Input.GetAxis("Horizontal");
        }

        // Check to see if the right mouse button is pressed

        // Get the difference in horizontal mouse movement
        x = Input.GetAxis("Mouse X") * turnSpeed * mouseTurnMultiplier;


        // rotate the character based on the x value
        transform.Rotate(0, x, 0);
    }
}
